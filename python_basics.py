# Основы Python

import random
import string


def generate_passwords(password_count, password_length_min, password_length_max):
    try:
        passwords = {}
        password_length = random.randint(password_length_min, password_length_max)

        # Словари
        letters_special = "+-/*!&$#?=@<>"
        letters_ascii = string.ascii_lowercase
        letters_digits = string.digits
        letters_ru = "".join([chr(i) for i in range(ord('а'), ord('а') + 32)])

        for i in range(password_count):
            password = ""
            while len(password) < password_length:
                # Берем символ из случайного словаря
                symbol = "".join(random.choice((letters_ascii, letters_ru, letters_special, letters_digits)
                                               [random.randint(0, 3)]))
                if random.getrandbits(1):
                    symbol = symbol.lower()
                else:
                    symbol = symbol.upper()
                password += symbol
            passwords[i] = password

        return passwords
    except Exception as ex:
        print(ex)


if __name__ == "__main__":
    print(generate_passwords(3, 6, 20))
