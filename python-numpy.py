# Вектор средних значений

import numpy as np


# Загрузка данных из файла
def load_array_fromfile(filename):
    try:
        data = np.loadtxt(filename, delimiter=",", skiprows=1)
        return data
    except Exception as e:
        print(str(e))


# Вывод вектора мин. значений
def print_min(narray):
    try:
        vector = []
        for i in range(len(narray)):
            vector.append(narray[i].min())
        print("Вектор мин. значений: " + str(vector))
    except Exception as e:
        print(str(e))


# Вывод вектора макс. значений
def print_max(narray):
    try:
        vector = []
        for i in range(len(narray)):
            vector.append(narray[i].max())
        print("Вектор макс. значений: " + str(vector))
    except Exception as e:
        print(str(e))


# Вывод вектора сумм
def print_sum(narray):
    try:
        vector = []
        for i in range(len(narray)):
            vector.append(narray[i].sum())
        print("Вектор сумм: " + str(vector))
    except Exception as e:
        print(str(e))


# Вывод вектора стредних значений
def print_average(narray):
    try:
        vector = []
        for i in range(len(narray)):
            vector.append(narray[i].sum() / len(narray[i]))
        print("Вектор средних значений: " + str(vector))
    except Exception as e:
        print(str(e))


if __name__ == "__main__":
    array = load_array_fromfile("boston_houses.csv")
    print_min(array)
    print_max(array)
    print_sum(array)
    print_average(array)
